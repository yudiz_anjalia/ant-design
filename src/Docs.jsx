import React from 'react'

export const Docs = () => {
  return (
    <div>
        <p>
Ant Design of React
Following the Ant Design specification, we developed a React UI library antd that contains a set of high quality components and demos for building rich, interactive user interfaces.

+
✨ Features
Environment Support
Version
Installation
Usage
Links
Companies using antd
Contributing
Need Help?
✨ Features#
🌈 Enterprise-class UI designed for web applications.

📦 A set of high-quality React components out of the box.

🛡 Written in TypeScript with predictable static types.

⚙️ Whole package of design resources and development tools.

🌍 Internationalization support for dozens of languages.

🎨 Powerful theme customization in every detail.

Environment Support#
Modern browsers and Internet Explorer 11 (with polyfills)

Server-side Rendering

Electron
        </p>
    </div>
  )
}
export default Docs;

