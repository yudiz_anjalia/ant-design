import React from 'react';
import './App.css';
import { Breadcrumb, Layout, Menu  } from 'antd';
import Title from 'antd/lib/typography/Title'
import { Avatar } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import {NavLink } from 'react-router-dom'
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem';
import Docs from './Docs';

const { Header, Footer, Sider, Content } = Layout;

function Dashboard() {
  return (
    <div>
      <Layout>
        <Header style={{padding:10}}>
        <Avatar style={{float:"right"}} src="./girl.png"/>
        <Title style={{color : "paleturquoise"}} level={3}>Example of Ant Design</Title>
        </Header>
        <Layout>
      <Sider>
        <Menu defaultSelectedKeys={['Dashboard']}
         mode="inline">
           
          <Menu.Item key='Dashboard'>
            Dashboard
          </Menu.Item>
          <SubMenu title ={
              <span>
                < title type="mail"/>
                <span>Ant-Design</span>
              </span>
            }> 
             <Menu.ItemGroup title="">
                <Menu.Item key='/docs'> Docs </Menu.Item>
                <Menu.Item key="Option2" > Component</Menu.Item>
             </Menu.ItemGroup>
          </SubMenu>
          <SubMenu title ={
              <span>
                < title type="mail"/>
                <span>Form</span>
              </span>
            }> 
             <Menu.ItemGroup title="">
                <Menu.Item key='Option1' > Login  </Menu.Item>
                <Menu.Item key="Option2" > Sign-Up </Menu.Item>
             </Menu.ItemGroup>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout>
        <Content style={{padding:'0 50px'}}>
          <Breadcrumb style={{margin:'16px 0'}}>
            <Breadcrumb.Item>Context of Ant-Design</Breadcrumb.Item>
            
          </Breadcrumb>
          <div style={{background :'black' , padding:24, minHeight:580}}><Docs/></div>
        </Content>
        <Footer style={{textAlign:'center'}}>Ant Design @2022 Craeted by Anjali</Footer>
        </Layout>
      </Layout>
    </Layout>
    </div>

    
  );
}

export default Dashboard;
